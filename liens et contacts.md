---
title: "Liens et contacts"
order: 1
in_menu: true
---
- HAL : [https://cv.hal.science/pierre-fleurence](https://cv.hal.science/pierre-fleurence)
- Mastodon : [https://pouet.chapril.org/@zucchi](https://pouet.chapril.org/@zucchi)
- Mail : [site@pierrefleurence.eu](mailto:site@pierrefleurence.eu) 