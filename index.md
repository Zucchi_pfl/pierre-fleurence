---
title: "Accueil"
order: 0
in_menu: true
---
# PIERRE FLEURENCE

Bienvenue sur ce site léger réalisé grâce à [Scribouilli](https://scribouilli.org/) et hébergé sur [gitlab.com](https://gitlab.com/) ! Vous y retrouverez des informations diverses sur moi et sur mes activités. Moi, c'est Pierre, ou Zucchi, au choix.

## Bio

Je suis ingénieur de recherche, spécialiste en informatique musicale et ingénieur du son. En parallèle de mon Master Image et Son à Brest, j'ai suivi des cours d'électroacoustique avec Noémie Springer et obtenu mon Certificat d'Études Musicales (CEM) en 2018.

Pendant mes études, je me suis consacré à la création sonore et à la lutherie numérique, collaborant avec divers artistes tels que Philippe Ollivier, Olivier Sens, Pascal Quéru, ainsi que des radios comme JetFM à Nantes. J'ai contribué au développement de logiciels comme Usine (Brainmodular, Paris) et Logelloop (Logelloù, Penvenan) en réalisant des missions centrées sur l'expérience utilisateur et la communication avec des capteurs/actionneurs. Ces activités m'ont amené à  présenter une pièce mixte avec Etienne Démoulin et Christian Barraza Godinez au festival Electrocution 2019 à Brest.

Après mon master, je me suis installé à Marseille en 2019 et ai commencé à travailler régulièrement au [GMEM (Centre National de Création Musicale, Marseille)](https://gmem.org/) en tant que réalisateur en informatique musicale et régisseur son. J'y ai  accompagné divers artistes sur des questions de spatialisation du son et de lutherie augmentée, et assuré l'entretien et l'organisation des équipements d'informatique musicale. En parallèle, j'ai enseigné les logiciels Ableton Live et Max aux étudiants du lycée Blaise Pascal en formation DNMADE.

En septembre 2020, j'ai débuté une thèse sous convention CIFRE en collaboration avec le GMEM et le laboratoire [PRISM (AMU, CNRS)](https://prism.cnrs.fr/) sur le contrôle perceptif de l’espace sonore. Actuellement, je travaille au laboratoire PRISM en tant qu'ingénieur de recherche spécialisé en audio 3D.


## Centres d'intérêts

Je suis intéressé par le monde du sonore en général et plus particulièrement par les questions de perception de ce monde. Je prend du plaisir à découvrir de nouvelles expériences auditives mais aussi à les discuter. Un vecteur audible que j'affectionne particulièrement est la radio qui me permet de me plonger dans des univers différents à tout moment.

Sur un autre registre, je suis très sensible aux questions d'autonomisation aux outils numériques, de protection de la vie privée et d'émancipation individuelle. Les engagements d'entités telles que [Framasoft](https://framasoft.org/), [La Quadrature du Net](https://www.laquadrature.net/), les [CHATONS](https://www.chatons.org/) ou encore [l'April](https://april.org/) m'inspirent beaucoup. J'essaye de participer, à mon échelle, à sensibiliser mes proches à ces questions en mettant en place des solutions techniques logicielles et en vulgarisant ces procédés (espaces communs numériques, protection individuelle, esprit critique). D'ailleurs, j'ai été interviewé à ce sujet dans l'émission "J'ai creusé pour vous" de [Thomas Chartier](https://dequevolem.com/) et il en est sorti deux podcast d'une heure où l'on aborde les pourquoi et les comment de cet engagement :

- [Vie privée numérique, 2) : mail, messagerie instantanée, cloud](https://ram05.fr/podcasts/jai-creuse-pour-vous/n05-vie-privee-numerique-partie-2-mail-messagerie-instantanee-cloud)
- [Vie privée numérique, 3) : Navigation Internet, vidéos, applis, OS](https://ram05.fr/podcasts/jai-creuse-pour-vous/n06-vie-privee-numerique-3-navigation-internet-videos-applis-os)

Je suis aussi attiré par les organisations collectives au quotidien et notamment dans le monde du travail que j'appréhende comme une part importante de ma vie. Le modèle des [CAE (Coopératives d'Activités et d'Emplois)](https://www.les-cae.coop/) me parle tout particulièrement grâce aux possibilités d'avancer à plusieurs tout en gardant une activité individuelle. 



*Dernière mise à jour : 06 Juin 2024* 