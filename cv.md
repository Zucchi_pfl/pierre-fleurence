---
title: "CV"
order: 1
in_menu: true
---
## Formation initiale
**2013**
- Baccalauréat Scientifique Section Orientale mention Assez Bien, lycée Jules Verne, Nantes.

**2013-2015**
- Licence 1 et 2 Sciences pour l’Ingénieur Parcours Électronique et Génie Civil, Univ. Nantes.

**2015-2016**
- Licence Sciences pour l’Ingénieur Parcours Image et Son mention Assez Bien, Univ. Bretagne Occidentale.

**2015-2018**
- Classe d'électroacoustique du CRR de Brest avec Noémie Springer, obtention du CEM en 2018.

**2017-2018**
- Master Image et Son, mention Assez Bien, Univ. Bretagne Occidentale.

**2020-2024 (prévision)**
- Thèse de Doctorat en Acoustique, Aix-Marseille Université.

## Formation continue
**2021**
- Ethique de la recherches, FUN Campus, En ligne
- Intégrité scientifique dans les métiers de la recherche, FUN Campus, En ligne
- Initiation au logiciel R, Statistiques et Analyse multivariée, ADUM, Marseille
- Rédiger sa bibliographie de thèse avec Zotero, ADUM, Aix-en-Provence

**2022**
- Agir contre les violences sexistes et sexuelles au travail, AVFT, Marseille

**2023**
- Gestion de projet agile à l’ère du numérique, ADUM, Marseille

## Expériences
### Création artistique et spectacle vivant
#### Réalisation informatique musicale
**Avril – Mai 2018**
- Stage à la Muse en Circuit, CNCM. Co-encadrement par Sébastien Béranger, responsable pédagogie et recherche, et Camille Lezer, ingénieur du son. Réflexion sur l’intuitivité des outils d’informatique musicale et développement de nouveaux outils à destination pédagogique sur le logiciel Usine.

**Juillet – Septembre 2018**
- Stage au GMEM, CNCM. Mise en pratique de technologies son 3D pour la création artistique (HOA, VBAP, DBAP).

**2018 – 2019**
- Travail sur développement du logiciel Logelloop.

**Décembre 2018 – Juin 2020**
- Assistance de différents compositeurs (Christian Sebille, Sarah Lianne Lewis, Alex Grillo, Habiba Effat, Lucien Gaudion, Alessandro Bosetti …) pour l’intégration de l’électronique dans leurs compositions et réalisation de pièces (Fausto Romitelli, Kaija Saariaho) en concerts. Développement de systèmes de spatialisation et de synthèse granulaire sous Max/MSP pour l’interprétation en temps réel. Accompagnement de projets autour de la lutherie augmentée (Floy Krouchi, François Wong).

#### Technique
**Août 2017** 
- Technicien lumière et poursuiteur, Festival Jazz In Marciac.

**Septembre 2017 – Août 2018**
- Régisseur lumière, Spectacle « Ron et ses Cuivrettes ».

**Août 2018**
- Régisseur son, Festival « LoopFest », Logelloù, Penvenan.

**Depuis Août 2018**
- Divers enregistrements d’ensembles instrumentaux et vocaux (musique rock, musique traditionnelle, musique sacrée, musique expérimentale, …).

**Décembre 2019 – Juin 2020**
- Régisseur son, GMEM. Préparation des résidences en cours, accompagnement des artistes sur les questions techniques liées à la scène et à la diffusion, mise en œuvre technique de la cabine d’enregistrement, maintien réseau audio numérique Dante entre les espaces du GMEM, maintien quotidien des espaces techniques et de création.

**Mars 2020 – Juin 2022**
- Intervenant vacataire au DNMADE “Régisseur du spectacle vivant”, formation aux logiciels Ableton Live et Max.

**Depuis Juin 2021**
- Organisation technique de plateaux radio nomades sur batterie dans le cadre du projet Viberation, collaboration avec François Wong. Viberation est une série de concerts sous casque (binaural ou non) qui prend place dans et aux alentours de la ville de Marseille. Lors des Viberation, le public est invité à écouter des pièces électroacoustiques.

### Recherche
**Novembre 2018**
- Ingénieur d’étude, LAB-STICC, Univ. Bretagne Occidentale : Apport du timbre sur la localisation en élévation. Mise en place d’un protocole expérimental et réalisation dans la chambre anéchoïque d’Orange Labs à Lannion sous la supervision de Mathieu Paquier et Rozenn Nicol.

**Septembre 2020 – Octobre 2024**
- Réalisation d’une thèse sous convention CIFRE en psychoacoustique sur le contrôle perceptif des espaces sonores avec le laboratoire PRISM (Aix Marseille Univ., CNRS) et le GMEM.

**Mai 2022 – Juin 2022**
- Encadrement d’un stage sur le développement de la documentation d’outils de synthèse granulaire, les [GMU](https://github.com/gmem-cncm/GMU).

**Avril 2023 – Septembre 2023**
- Co-encadrement d’un stage sur les enjeux et techniques de sonification.

**Depuis septembre 2023**
- Ingénieur de recherche, CNRS, laboratoire PRISM. Gestion de projet et conception d’une plateforme d’immersion multimodale à destination de la recherche, du développement et de la création artistique. 



*Dernière mise à jour : 06 Juin 2024* 